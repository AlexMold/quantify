var express = require('express');
var app = new express();
var path = require('path');
// var bodyParser = require('body-parser');
var port = 3000

app.use('/dist', express.static(path.join(__dirname, 'dist')))

app.get("/", function(req, res) {
    res.sendFile(__dirname + '/index.html')
})

app.listen(port, function(error) {
    if (error) {
        console.error(error)
    } else {
        console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port)
    }
})