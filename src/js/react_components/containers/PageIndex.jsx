import React, { Component } from 'react'
import ContainerApp from './ContainerApp'
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import * as reducers from '../reducers';


const reducer = combineReducers(reducers);
const store = createStore(reducer);


class PageIndex extends Component {

    constructor(props, context) {
        super(props, context)
    }

    render() {
        return (
            <Provider store={store}>
                <ContainerApp />
            </Provider>
        )

    }
}

export default PageIndex