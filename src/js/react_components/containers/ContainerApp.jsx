import React, { Component } from 'react'
import { Container, Header } from 'semantic-ui-react'
import HeaderApp from '../components/HeaderApp'
import FormAdd from '../components/FormAdd'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as TitleActions from '../actions/index'


class ContainerApp extends Component {

    constructor(props, context) {
        super(props, context)
    }

    render() {
        return (
            <Container>
                <HeaderApp {...this.props} />
                <br/>
                <FormAdd {...this.props} />
            </Container>
        )

    }
}

const mapStateToProps = (state, props) => {
    console.log(state)
    return { list: state.default.notifications, showNote: state.default.popup.showNote }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    handleRead: () => {
        dispatch(TitleActions.allRead())
    },
    deleteAll: () => {
        dispatch(TitleActions.deleteAll())
    },
    togglePopup: () => {
        dispatch(TitleActions.togglePopup())
    },
    addTitle: (formData) => {
        dispatch(TitleActions.addTitle(formData))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContainerApp)