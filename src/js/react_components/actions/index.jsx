import * as types from '../constants/actionTypes';

export const addTitle = ({ title }) => ({ type: types.ADD_TITLE, title });

export const allRead = () => ({ type: types.ALL_READ, unread: false });

export const deleteAll = () => ({ type: types.DELETE_ALL });

export const togglePopup = bool => ({ type: types.TOGGLE_POPUP, bool });
