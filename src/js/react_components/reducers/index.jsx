import { combineReducers } from 'redux';
import notifications from './notifications';
import popup from './popup';


export default combineReducers({
  notifications,
  popup
});