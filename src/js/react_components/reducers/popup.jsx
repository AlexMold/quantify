import * as types from "../constants/actionTypes";

const initialState = { showNote: false };

export default function popup(state = initialState, action) {
    switch (action.type) {
        case types.TOGGLE_POPUP:
            return { showNote: !state.showNote }

        default:
            return state;
    }
}