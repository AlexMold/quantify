import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import PageIndex from './containers/PageIndex';

render(
    <PageIndex />,
    document.getElementById('root')
)