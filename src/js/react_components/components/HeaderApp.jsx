import React, {Component} from 'react'
import { Menu } from 'semantic-ui-react'
import Notificator from './Notificator'


class HeaderApp extends Component {

    constructor(props, context) {
        super(props, context)
    }

    render() {
        return (
            <Menu>
                <Menu.Item position="right">
                    <Notificator 
                        {...this.props} />
                </Menu.Item>
            </Menu>
        )
    }
}

export default HeaderApp