import React, { Component } from 'react';
import { Button, Form, Input, Grid } from 'semantic-ui-react';

class FormAdd extends Component {

    constructor(props, context) {
        super(props, context)

        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit(e, { formData }) {
        const { addTitle } = this.props;
        e.preventDefault()
        console.log(' formData ', formData)
        
        addTitle(formData)
    }

    render() {
        const { handleRead, deleteAll, togglePopup } = this.props
        return (
            <div>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group>
                        <Form.Field>
                            <Input
                                name='title'
                                placeholder='Ввести название' />
                        </Form.Field>                
                        <Form.Field>
                            <Button
                                type='submit'>
                                Отправить
                            </Button>
                        </Form.Field>                
                    </Form.Group>
                </Form>

                <Button.Group basic vertical>
                    <Button onClick={handleRead}>Пометить все события прочитанными</Button>
                    <Button onClick={deleteAll}>Удалить все события</Button>
                    <Button onClick={togglePopup}>Скрыть / показать попап нотификаций</Button>
                </Button.Group>
            </div>
        )
    }
}

export default FormAdd