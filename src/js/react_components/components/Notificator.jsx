import React, { Component } from 'react'
import { Popup, Icon } from 'semantic-ui-react'
import NotificatorList from './NotificatorList'


class Notificator extends Component {

    constructor(props, context) {
        super(props, context)
    }

    render() {
        const { list, handleRead, showNote, togglePopup } = this.props;
        const unreadList = list.filter(item => item.unread);

        console.log(' Notificator showNote ', showNote)
        return (
            <div>
                <Popup
                    trigger={
                        <div className="notification_icon">
                            <Icon 
                                name="alarm" 
                                onClick={togglePopup}
                                />
                            <span>{unreadList.length ? unreadList.length : null}</span>
                        </div>
                    }
                    content={
                        <NotificatorList 
                            className="origin-name"
                            unreadList={unreadList}
                            {...this.props} />
                    }
                    open={showNote}
                    on="click"
                    onClose={handleRead}
                    positioning="bottom right"
                    className="notification_wrapper" />
                
            </div>
        )
    }
}

export default Notificator