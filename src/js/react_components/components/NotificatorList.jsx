import React, { Component } from 'react'

import '../../../styl/notification_list.styl'

const renderList = (list) => list.map(item => item.id ? <li key={item.id}>{item.title}</li> : null)
const handlePopup = ({ handleRead, togglePopup }) => {
    handleRead();
    togglePopup();
}

export default class NotificatorList extends Component {

    constructor(props, context) {
        super(props, context)
    }

    render() {
        const { unreadList, list, togglePopup } = this.props;
        return (
            <ul className="notification_list">
                {
                    unreadList.length ? 
                    renderList(unreadList.length > 5 ? unreadList.slice(-5) : unreadList) : 
                    renderList(list.length > 5 ? list.slice(-5) : list)
                }
                <a onClick={handlePopup.bind(this, this.props)} href="#">Посмотреть все...</a>
            </ul>
        )
    }
}